FROM amazoncorretto:11-alpine-jdk
MAINTAINER victor.abidoye
COPY target/walletAccount-service-0.0.1-SNAPSHOT.jar waller-service-0.0.1.jar
ENTRYPOINT ["java","-jar","/waller-service-0.0.1.jar"]