package com.clane.walletservice.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@ConfigurationProperties(prefix = "app")
public class AppProperties {
    @NotEmpty(message = "app.key cannot be empty")
    private String key;
    @NotEmpty
    private String redirectUrl;
    @NotEmpty(message = "app.confirmationCacheTTL cannot be empty")
    private Integer confirmationCacheTTL;
}
