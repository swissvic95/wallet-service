package com.clane.walletservice.config;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.time.Duration;

/**
 * @author victor abidoye
 */
@Configuration
@AllArgsConstructor
public class RedisConfig {

    private ReactiveRedisConnectionFactory reactiveRedisConnectionFactory;

    @Bean
    public ReactiveRedisTemplate<String, Object> reactiveRedisTemplate() {
        StringRedisSerializer keySerializer = new StringRedisSerializer();
        Jackson2JsonRedisSerializer<Object> valueSerializer = new Jackson2JsonRedisSerializer<Object>(Object.class);
        RedisSerializationContext.RedisSerializationContextBuilder<String, Object> builder = RedisSerializationContext.
                newSerializationContext(keySerializer);
        RedisSerializationContext<String, Object> context = builder.value(valueSerializer).build();

        ReactiveRedisTemplate<String, Object> redisTemplate = new ReactiveRedisTemplate<>(reactiveRedisConnectionFactory,
                context);

        return redisTemplate;
    }
}
