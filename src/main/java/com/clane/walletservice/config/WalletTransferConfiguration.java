package com.clane.walletservice.config;

import com.clane.walletservice.domain.Money;
import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @author victor abidoye
 */
@Data
@Component
public class WalletTransferConfiguration {
    private Money KYCSilverTransferThreshold = Money.of(50000L);
}
