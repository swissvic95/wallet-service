package com.clane.walletservice.controller;

import com.clane.walletservice.controller.request.TopUpRequest;
import com.clane.walletservice.controller.response.Response;
import com.clane.walletservice.controller.response.WalletAccountResponse;
import com.clane.walletservice.service.AccountManagementService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author victor abidoye
 */
@RestController
@RequestMapping("/v1/wallet")
@AllArgsConstructor
public class AccountManagerController {

    private AccountManagementService accountManagementService;

    @PostMapping(value = "/top-up", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Mono<Response> topUpAccount(@RequestBody @Validated TopUpRequest request) {
        return accountManagementService.topUpAccount(request);
    }

    @GetMapping(value = "/{walletAccountId}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<WalletAccountResponse> getWalletAccount(@PathVariable String walletAccountId) {
        return accountManagementService.getWalletAccountById(walletAccountId);
    }
}
