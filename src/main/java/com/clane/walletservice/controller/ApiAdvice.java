package com.clane.walletservice.controller;

import com.clane.walletservice.controller.response.Error;
import com.clane.walletservice.controller.response.Response;
import com.clane.walletservice.controller.response.ResponseCode;
import com.clane.walletservice.exception.DuplicateRecordException;
import com.clane.walletservice.exception.NotFoundException;
import com.clane.walletservice.exception.RequestException;
import com.clane.walletservice.exception.TransactionProcessException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.server.ServerWebInputException;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author victor abidoye
 */
@ControllerAdvice(annotations = RestController.class)
@ResponseBody
@Slf4j
public class ApiAdvice {

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public Response methodArgumentNotValidException(MethodArgumentNotValidException e){
        log.error(e.getMessage(), e);
        BindingResult result = e.getBindingResult();
        List<ObjectError> objectErrors = result.getAllErrors();
        Response baseResponse = new Response();
        baseResponse.setResponseCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
        baseResponse.setResponseDescription(ResponseCode.BAD_REQUEST.getResponseMessage());
        List<Error> errorList = new ArrayList<>();
        for (ObjectError objectError : objectErrors){
            if (objectError instanceof FieldError){
                FieldError fieldError = (FieldError) objectError;
                errorList.add(new Error(fieldError.getField(), fieldError.getDefaultMessage()));
            }
            else {
                errorList.add(new Error(objectError.getObjectName(), objectError.getDefaultMessage()));
            }
        }
        baseResponse.setErrors(errorList);

        return baseResponse;
    }

    @ExceptionHandler({HttpMessageNotReadableException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
        log.error(e.getMessage(), e);
        Response response = new Response();
        response.setResponseCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
        response.setResponseDescription(e.getLocalizedMessage());

        if (e.getCause() != null) {
            String message = e.getCause().getMessage();
            if (e.getCause() instanceof JsonMappingException) {
                String[] arr = message.split("\\(");
                if (arr.length > 0) {
                    String temp = arr[0];
                    String[] arr2 = message.split("\\[");
                    if (arr2.length > 1) {
                        message = temp + " (fieldName: [" + arr2[1];
                    } else {
                        message = temp;
                    }
                }
            }

            if (e.getCause() instanceof JsonParseException) {
                String[] arr = message.split("at");
                if (arr.length > 0) {
                    String temp = arr[0];
                    JsonParseException jpe = (JsonParseException) e.getCause();
                    message = temp + " [line: " + jpe.getLocation().getLineNr() + ", column: " +
                            jpe.getLocation().getColumnNr() + "]";
                }
            }
            response.setResponseDescription(message);
        }

        return response;
    }

    @ExceptionHandler({WebExchangeBindException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response webExchangeBindException(WebExchangeBindException e) {
        log.error(e.getMessage(), e);
        Response response = new Response();
        List<Error> errors = new ArrayList<>();
        e.getBindingResult().getAllErrors().stream().sorted(ApiAdvice::compare)
                .forEach((violation) -> {
                    if (violation instanceof FieldError){
                        FieldError fieldError = (FieldError) violation;
                        errors.add(new Error(fieldError.getField(), fieldError.getDefaultMessage()));
                    }
                    else {
                        errors.add(new Error(violation.getObjectName(), violation.getDefaultMessage()));
                    }
                });

        response.setResponseCode(ResponseCode.BAD_REQUEST.getResponseCode());
        response.setErrors(errors);
        return response;
    }

    @ExceptionHandler({ServerWebInputException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response serverWebInputException(ServerWebInputException e) {
        log.error(e.getMessage(), e);
        Response baseResponse = new Response();
        baseResponse.setResponseCode(ResponseCode.BAD_REQUEST.getResponseCode());
        String responseMessage = StringUtils.isEmpty(e.getMessage()) ? ResponseCode.BAD_REQUEST.getResponseMessage() :
                e.getMessage().contains("Failed to read HTTP message") ? ResponseCode.BAD_REQUEST.getResponseMessage() :
                        e.getMessage();
        baseResponse.setResponseDescription(responseMessage);
        return baseResponse;
    }

    @ExceptionHandler({ConstraintViolationException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response serverWebInputException(ConstraintViolationException e) {
        log.error(e.getMessage(), e);
        String[] messages = e.getMessage().split("\\.");
        Response baseResponse = new Response();
        baseResponse.setResponseCode(ResponseCode.BAD_REQUEST.getResponseCode());
        baseResponse.setResponseDescription(messages[messages.length - 1]);
        return baseResponse;
    }

    @ExceptionHandler({Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Response genericException(Exception e) {
        log.error(e.getMessage(), e);
        Response response = new Response();
        response.setResponseCode(ResponseCode.SYSTEM_ERROR.getResponseCode());
        String responseMessage = ResponseCode.SYSTEM_ERROR.getResponseMessage();
        response.setResponseDescription(responseMessage);
        return response;
    }

    @ExceptionHandler({TransactionProcessException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response transactionException(TransactionProcessException e) {
        log.error(e.getMessage(), e);
        Response response = new Response();
        response.setResponseCode(e.getResponseCode().getResponseCode());
        response.setResponseDescription(e.getResponseCode().getResponseMessage());
        return response;
    }

    @ExceptionHandler({DuplicateRecordException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    public Response duplicateRecordException(DuplicateRecordException e) {
        log.error(e.getMessage(), e);
        Response baseResponse = new Response();
        baseResponse.setResponseCode(ResponseCode.CONFLICT.getResponseCode());
        String responseMessage = StringUtils.isEmpty(e.getMessage()) ? ResponseCode.CONFLICT.getResponseMessage() :
                e.getMessage();
        baseResponse.setResponseDescription(responseMessage);
        return baseResponse;
    }

    @ExceptionHandler({RequestException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Response handleRequestException(RequestException e){
        Response response = new Response();
        String responseMessage = StringUtils.isEmpty(e.getMessage()) ? ResponseCode.BAD_REQUEST.getResponseMessage() :
                e.getMessage();
        response.setResponseCode(ResponseCode.BAD_REQUEST.getResponseCode());
        response.setResponseDescription(responseMessage);
        return response;
    }

    @ExceptionHandler({NotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Response handleNotFoundException(NotFoundException e){
        Response response = new Response();
        String responseMessage = StringUtils.isEmpty(e.getMessage()) ? ResponseCode.NOT_FOUND.getResponseMessage() :
                e.getMessage();
        response.setResponseCode(ResponseCode.NOT_FOUND.getResponseCode());
        response.setResponseDescription(responseMessage);
        return response;
    }

    private static int compare(ObjectError e1, ObjectError e2) {
        return e1.getDefaultMessage().compareTo(e2.getDefaultMessage());
    }
}
