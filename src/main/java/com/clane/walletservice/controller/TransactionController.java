package com.clane.walletservice.controller;

import com.clane.walletservice.controller.request.SendMoneyRequest;
import com.clane.walletservice.controller.response.TransactionResponse;
import com.clane.walletservice.service.SendMoneyService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author victor abidoye
 */
@RestController
@RequestMapping("/v1/transaction")
@AllArgsConstructor
public class TransactionController {

    private SendMoneyService sendMoneyService;

    @PostMapping(value = "/send-money", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Mono<TransactionResponse> sendMoney(@RequestBody @Validated SendMoneyRequest request) {
        return sendMoneyService.sendMoney(request);
    }
}
