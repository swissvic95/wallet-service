package com.clane.walletservice.controller;

import com.clane.walletservice.controller.request.UserProfilePayload;
import com.clane.walletservice.controller.response.Response;
import com.clane.walletservice.controller.response.UserProfileResponse;
import com.clane.walletservice.service.UserService;
import com.clane.walletservice.validation.ValidationGroup.ValidateGetUserByEmail;
import com.clane.walletservice.validation.ValidationGroup.ValidateUpdateUserRecordRequest;
import com.clane.walletservice.validation.ValidationGroup.ValidateUserCreationRequest;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import javax.validation.constraints.NotEmpty;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author victor abidoye
 */
@RestController
@RequestMapping("/v1/user")
@AllArgsConstructor
public class UserController {

    private UserService userService;

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<UserProfileResponse> registerUser(@Validated(ValidateUserCreationRequest.class) @RequestBody
                                                              UserProfilePayload request) {
        return userService.registerNewUser(request);
    }

    @PutMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Mono<UserProfileResponse> updateUserProfile(@Validated(ValidateUpdateUserRecordRequest.class) @RequestBody
                                                                   UserProfilePayload request) {
        return userService.updateUserProfile(request);
    }

    @PostMapping(value = "/wallet/activate", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Mono<Response> activateWallet(@RequestBody @NotEmpty(message = "request requires the confirmationData")
                                                     String confirmationData) {
        return userService.activateWalletUsingUserConfirmationData(confirmationData);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Mono<UserProfileResponse> getUserByEmail(@Validated(ValidateGetUserByEmail.class) @RequestBody
                                                                UserProfilePayload request) {
        return userService.getUserByEmail(request.getEmail());
    }
}
