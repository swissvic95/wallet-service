package com.clane.walletservice.controller.request;

import com.clane.walletservice.validation.annotation.ValidSendMoneyRequest;
import lombok.Data;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

/**
 * @author victor abidoye
 */
@Data
@ValidSendMoneyRequest(message = "You must include either the email or walletAccountId of source and target account")
public class SendMoneyRequest {
    @Email
    private String sourceEmail;
    private String sourceWalletAccountId;

    @Email
    private String targetEmail;
    private String targetWalletAccountId;
    @NotNull(message = "amount cannot be empty")
    private Long amount;
}
