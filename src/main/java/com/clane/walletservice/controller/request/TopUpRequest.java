package com.clane.walletservice.controller.request;

import com.clane.walletservice.validation.annotation.ValidNonNullBetweenTwoFields;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

/**
 * @author victor abidoye
 */
@Data
@ValidNonNullBetweenTwoFields(fieldNames = {"email", "walletAccountId"},
        message = "You must include either the email or walletAccountId of account")
public class TopUpRequest {
    private String walletAccountId;
    @Email
    private String email;
    @NotNull
    private Long amount;
}
