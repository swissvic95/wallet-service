package com.clane.walletservice.controller.request;

import com.clane.walletservice.domain.Gender;
import com.clane.walletservice.validation.ValidationGroup;
import com.clane.walletservice.validation.ValidationGroup.ValidateGetUserByEmail;
import com.clane.walletservice.validation.ValidationGroup.ValidateUpdateUserRecordRequest;
import com.clane.walletservice.validation.ValidationGroup.ValidateUserCreationRequest;
import com.clane.walletservice.validation.annotation.ValidEnumValue;
import com.clane.walletservice.validation.annotation.ValidPhoneNumber;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.*;
import java.time.LocalDate;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserProfilePayload {
    @NotEmpty(message = "firstName cannot be null or empty",
            groups = {ValidateUserCreationRequest.class, ValidateUpdateUserRecordRequest.class})
    private String firstName;

    @NotEmpty(message = "lastName cannot be null or empty",
            groups = {ValidateUserCreationRequest.class, ValidateUpdateUserRecordRequest.class})
    private String lastName;

    @Email(message = "wrong email format",
            groups = {ValidateUserCreationRequest.class, ValidateUpdateUserRecordRequest.class})
    @NotEmpty(message = "email cannot be null or empty",
            groups = {ValidateUserCreationRequest.class, ValidateUpdateUserRecordRequest.class,
                    ValidateGetUserByEmail.class})
    private String email;

    @ValidPhoneNumber(groups = {ValidateUserCreationRequest.class, ValidateUpdateUserRecordRequest.class})
    private String phoneNumber;

    @NotEmpty(message = "password cannot be null or empty", groups = ValidateUserCreationRequest.class)
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[@#$%^&+=])(?=\\S+$).{5,}$",
            message = "password must have at lease one digit, a letter, a special character, " +
                    "and must have at least 5 characters", groups = ValidateUserCreationRequest.class)
    private String password;

    @NotNull(message = "gender cannot be null or empty", groups = ValidateUpdateUserRecordRequest.class)
    @ValidEnumValue(message = "gender can only be either MALE or FEMALE", enumClass = Gender.class,
            groups = ValidateUpdateUserRecordRequest.class)
    private String gender;
    @NotNull(message = "dateOfBirth cannot be null or empty", groups = ValidateUpdateUserRecordRequest.class)
    @Past(groups = ValidateUpdateUserRecordRequest.class)
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate dateOfBirth;
    @NotEmpty(message = "addressOne cannot be null or empty", groups = ValidateUpdateUserRecordRequest.class)
    private String addressOne;
    private String addressTwo;
    @NotEmpty(message = "country cannot be null or empty", groups = ValidateUpdateUserRecordRequest.class)
    private String country;
    @NotEmpty(message = "state cannot be null or empty", groups = ValidateUpdateUserRecordRequest.class)
    private String state;
    @NotEmpty(message = "city cannot be null or empty", groups = ValidateUpdateUserRecordRequest.class)
    private String city;
    private String redirectUrl;

    public Gender getGender() {
        return this.gender == null ? null : Gender.valueOf(gender.toUpperCase());
    }
}