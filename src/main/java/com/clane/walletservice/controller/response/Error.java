package com.clane.walletservice.controller.response;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author victor abidoye
 */
@Data
@AllArgsConstructor
public class Error {
    private String field;
    private String message;
}
