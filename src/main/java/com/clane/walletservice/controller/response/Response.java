package com.clane.walletservice.controller.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class Response {
    private String responseCode;
    private String responseDescription;
    private List<Error> errors;
}
