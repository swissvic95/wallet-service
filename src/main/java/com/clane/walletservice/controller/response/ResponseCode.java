package com.clane.walletservice.controller.response;

public enum ResponseCode {
    SUCCESSFUL("00", "SUCCESSFUL"),
    CONFLICT("01", "CONFLICT"),
    NOT_FOUND("02", "NOT_FOUND"),
    BAD_REQUEST("04", "BAD_REQUEST"),
    SYSTEM_ERROR("05", "SYSTEM_ERROR"),
    INSUFFICIENT_FUND("06", "INSUFFICIENT_FUND"),
    SOURCE_ACCOUNT_INACTIVE("07", "SOURCE_ACCOUNT_INACTIVE"),
    THRESHOLD_EXCEEDED("08", "THRESHOLD_EXCEEDED");

    private String responseCode;
    private String responseMessage;

    ResponseCode(String responseCode, String responseMessage) {
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }
}
