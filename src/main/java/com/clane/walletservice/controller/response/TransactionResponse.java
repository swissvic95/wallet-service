package com.clane.walletservice.controller.response;

import com.clane.walletservice.domain.TransactionStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author victor abidoye
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionResponse extends Response{
    private Long amount;
    private String transactionRef;
    private TransactionStatus transactionStatus;
    private String sourceWalletAccountId;
    private String fromWalletAccountId;
    private LocalDateTime transactionDateTime;
}
