package com.clane.walletservice.controller.response;

import com.clane.walletservice.domain.Gender;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.time.LocalDate;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserProfileResponse extends Response{
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String walletId;
    private Gender gender;
    private LocalDate dateOfBirth;
    private String addressOne;
    private String addressTwo;
    private String country;
    private String state;
    private String city;
}
