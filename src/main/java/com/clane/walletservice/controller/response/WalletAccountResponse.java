package com.clane.walletservice.controller.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author victor abidoye
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WalletAccountResponse {
    private String walletAccountId;
    private Long balance;
    private boolean active;
    private LocalDateTime dateTimeCreated;
}
