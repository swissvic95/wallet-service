package com.clane.walletservice.dataaccess;

import com.clane.walletservice.dataaccess.model.Transaction;
import com.clane.walletservice.dataaccess.model.WalletAccount;
import com.clane.walletservice.domain.Account;
import com.clane.walletservice.domain.Activity;
import com.clane.walletservice.domain.Money;
import org.springframework.stereotype.Component;

/**
 * @author victor abidoye
 */
@Component
public class AccountMapper {
    public Account mapToAccountDomainEntity(WalletAccount walletAccount) {
        Money balance = Money.of(walletAccount.getBalance());
        return Account.withId(
                walletAccount.getWalletAccountId(),
                balance,
                walletAccount.isActive(),
                walletAccount.getUser().getKycLevel()
        );
    }

    public Transaction mapToTransactionJpaEntity(Activity activity) {
        return new Transaction(
                null,
                activity.getTransactionRef(),
                activity.getSourceAccount().getId().get(),
                activity.getTargetAccount().getId().get(),
                activity.getMoney().getAmount().longValue(),
                activity.getStatus(),
                activity.getTimestamp()
        );
    }
}
