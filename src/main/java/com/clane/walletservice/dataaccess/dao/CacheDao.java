package com.clane.walletservice.dataaccess.dao;

import com.clane.walletservice.domain.UserConfirmation;
import reactor.core.publisher.Mono;

/**
 * @author victor abidoye
 */
public interface CacheDao {
    Mono<Boolean> cacheConfirmationData(UserConfirmation userConfirmation);
    Mono<Object> get(String key);
    Mono<Boolean> remove(String key);
}
