package com.clane.walletservice.dataaccess.dao;

import com.clane.walletservice.dataaccess.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author victor abidoye
 */
public interface TransactionDao extends JpaRepository<Transaction, Long> {
}
