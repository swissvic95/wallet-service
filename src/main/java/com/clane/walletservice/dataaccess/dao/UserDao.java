package com.clane.walletservice.dataaccess.dao;

import com.clane.walletservice.dataaccess.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author victor abidoye
 */
public interface UserDao extends JpaRepository<User, Long> {
    User findByEmail(String email);
}
