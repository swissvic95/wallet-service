package com.clane.walletservice.dataaccess.dao;

import com.clane.walletservice.dataaccess.model.WalletAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author victor abidoye
 */
public interface WalletAccountDao extends JpaRepository<WalletAccount, String> {
    WalletAccount findByWalletAccountId(String id);

    @Transactional
    @Modifying
    @Query("update WalletAccount wc set wc.balance = :balance where wc.walletAccountId = :walletAccountId")
    void updateWalletAccountBalance(@Param(value = "balance") Long balance, @Param(value = "walletAccountId")
            String walletAccountId);
}