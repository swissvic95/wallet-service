package com.clane.walletservice.dataaccess.model;

import com.clane.walletservice.domain.TransactionStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author victor abidoye
 */

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String transactionRef;
    private String sourceWalletAccountId;
    private String fromWalletAccountId;
    private Long amount;
    @Enumerated(EnumType.STRING)
    private TransactionStatus transactionStatus;
    private LocalDateTime transactionDateTime;
}
