package com.clane.walletservice.dataaccess.model;

import com.clane.walletservice.domain.Gender;
import com.clane.walletservice.domain.KYCLevel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import javax.persistence.*;
import java.time.LocalDate;

/**
 * @author victor abidoye
 */

@Data
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String firstName;
    private String lastName;
    @Column(unique = false)
    private String email;
    private String phoneNumber;
    @Enumerated(EnumType.STRING)
    private Gender gender;
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate dateOfBirth;
    private String addressOne;
    private String addressTwo;
    private String country;
    private String state;
    private String city;
    @Enumerated(EnumType.STRING)
    private KYCLevel kycLevel;
    private String hashedPassword;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "wallet_account_id", referencedColumnName = "wallet_account_id")
    private WalletAccount walletAccount;
}
