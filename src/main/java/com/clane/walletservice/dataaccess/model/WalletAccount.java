package com.clane.walletservice.dataaccess.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author victor abidoye
 */

@Data
@Entity
@Table(name = "wallet_account")
public class WalletAccount {
    @Id
    @Column(name = "wallet_account_id")
    private String walletAccountId;
    private Long balance = 0L;
    private boolean active;
    private LocalDateTime dateTimeCreated;
    @OneToOne(mappedBy="walletAccount")
    @ToString.Exclude
    private User user;
}