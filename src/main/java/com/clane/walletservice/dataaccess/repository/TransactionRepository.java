package com.clane.walletservice.dataaccess.repository;

import com.clane.walletservice.dataaccess.model.Transaction;
import reactor.core.publisher.Mono;

/**
 * @author victor abidoye
 */
public interface TransactionRepository {
    Mono<Transaction> addTransaction(Transaction transaction);
}
