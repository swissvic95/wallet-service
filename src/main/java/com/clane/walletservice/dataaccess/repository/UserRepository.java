package com.clane.walletservice.dataaccess.repository;

import com.clane.walletservice.dataaccess.model.User;
import reactor.core.publisher.Mono;

import java.util.Optional;

/**
 * @author victor abidoye
 */

public interface UserRepository {
    Mono<User> addUser(User user);
    Mono<User> update(User user);
    Mono<User> getUserByEmail(String email);
    Mono<Optional<User>> getUserById(long id);
}
