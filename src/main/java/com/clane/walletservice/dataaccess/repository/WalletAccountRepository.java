package com.clane.walletservice.dataaccess.repository;

import com.clane.walletservice.dataaccess.model.WalletAccount;
import com.github.javafaker.Bool;
import reactor.core.publisher.Mono;

/**
 * @author victor abidoye
 */

public interface WalletAccountRepository {
    Mono<Boolean> updateWalletAccountBalance(Long balance, String walletAccountId);
    Mono<WalletAccount> loadWalletAccount(String walletAccountId, String email);
    Mono<WalletAccount> loadWalletAccountById(String walletAccountId);
}
