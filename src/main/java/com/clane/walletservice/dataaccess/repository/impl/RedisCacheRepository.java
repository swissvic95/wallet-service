package com.clane.walletservice.dataaccess.repository.impl;

import com.clane.walletservice.config.AppProperties;
import com.clane.walletservice.dataaccess.dao.CacheDao;
import com.clane.walletservice.domain.UserConfirmation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.time.Duration;

/**
 * @author victor abidoye
 */
@Repository
@Slf4j
@AllArgsConstructor
public class RedisCacheRepository implements CacheDao {

    private ReactiveRedisTemplate<String, Object> reactiveRedisTemplate;
    private AppProperties appProperties;

    @Override
    public Mono<Boolean> cacheConfirmationData(UserConfirmation userConfirmation) {
        String key = userConfirmation.getConfirmationData();
        reactiveRedisTemplate.expire(key, Duration.ofSeconds(appProperties.getConfirmationCacheTTL()));
        return reactiveRedisTemplate.opsForValue().set(key, userConfirmation.getUser().getEmail());
    }

    @Override
    public Mono<Object> get(String key) {
        return reactiveRedisTemplate.opsForValue().get(key);
    }

    @Override
    public Mono<Boolean> remove(String key) {
        return reactiveRedisTemplate.opsForValue().delete(key);
    }
}