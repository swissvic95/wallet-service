package com.clane.walletservice.dataaccess.repository.impl;

import com.clane.walletservice.dataaccess.dao.TransactionDao;
import com.clane.walletservice.dataaccess.model.Transaction;
import com.clane.walletservice.dataaccess.repository.TransactionRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

/**
 * @author victor abidoye
 */
@Repository
@AllArgsConstructor
public class TransactionRepositoryImpl implements TransactionRepository {

    private TransactionDao transactionDao;
    private Scheduler scheduler;

    @Override
    public Mono<Transaction> addTransaction(Transaction transaction) {
        return Mono.fromCallable(() -> transactionDao.save(transaction))
                .subscribeOn(scheduler);
    }
}
