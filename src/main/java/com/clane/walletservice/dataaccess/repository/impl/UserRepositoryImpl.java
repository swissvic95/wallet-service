package com.clane.walletservice.dataaccess.repository.impl;

import com.clane.walletservice.dataaccess.dao.UserDao;
import com.clane.walletservice.dataaccess.repository.UserRepository;
import com.clane.walletservice.dataaccess.model.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

import java.util.Optional;

/**
 * @author victor abidoye
 */

@Repository
@AllArgsConstructor
public class UserRepositoryImpl implements UserRepository {

    private UserDao userDao;
    private Scheduler scheduler;

    public Mono<User> addUser(User user) {
        return Mono.fromCallable(() -> this.userDao.save(user))
                .subscribeOn(scheduler);
    }

    @Override
    public Mono<User> update(User user) {
        return addUser(user);
    }

    @Override
    public Mono<User> getUserByEmail(String email) {
        return Mono.fromCallable(() -> this.userDao.findByEmail(email))
                .subscribeOn(scheduler);
    }

    @Override
    public Mono<Optional<User>> getUserById(long id) {
        return Mono.fromCallable(() -> this.userDao.findById(id))
                .subscribeOn(scheduler);
    }
}