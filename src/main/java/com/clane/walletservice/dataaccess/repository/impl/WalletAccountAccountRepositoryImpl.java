package com.clane.walletservice.dataaccess.repository.impl;

import com.clane.walletservice.dataaccess.dao.UserDao;
import com.clane.walletservice.dataaccess.dao.WalletAccountDao;
import com.clane.walletservice.dataaccess.model.WalletAccount;
import com.clane.walletservice.dataaccess.repository.WalletAccountRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

/**
 * @author victor abidoye
 */

@Repository
@AllArgsConstructor
public class WalletAccountAccountRepositoryImpl implements WalletAccountRepository {

    private WalletAccountDao walletAccountDao;
    private UserDao userDao;
    private Scheduler scheduler;

    @Override
    public Mono<Boolean> updateWalletAccountBalance(Long balance, String walletAccountId) {
        return Mono.fromCallable(() -> {
            walletAccountDao.updateWalletAccountBalance(balance, walletAccountId);
            return true;
        }).subscribeOn(scheduler);
    }

    public Mono<WalletAccount> loadWalletAccount(String walletAccountId, String email) {
        Mono<WalletAccount> walletAccount = Mono.empty();
        if (StringUtils.hasText(walletAccountId)){
            walletAccount = loadWalletAccountById(walletAccountId);
        }
        else if(StringUtils.hasText(email)) {
            walletAccount = loadWalletAccountByUserEmail(email);
        }

        return walletAccount;
    }

    public Mono<WalletAccount> loadWalletAccountById(String walletAccountId) {
        return Mono.fromCallable(() -> walletAccountDao.findByWalletAccountId(walletAccountId))
                .subscribeOn(scheduler);
    }

    private Mono<WalletAccount> loadWalletAccountByUserEmail(String email) {
        return Mono.fromCallable(() -> userDao.findByEmail(email))
                .flatMap(user -> Mono.just(user.getWalletAccount()))
                .subscribeOn(scheduler);
    }
}
