package com.clane.walletservice.domain;

import com.clane.walletservice.controller.response.ResponseCode;
import com.clane.walletservice.exception.TransactionProcessException;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.util.Optional;

/**
 * @author victor abidoye
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Account {
    /**
     * The unique ID of the account.
     */
    private final String accountId;
    private Money balance;
    private final boolean active;
    private KYCLevel kycLevel;

    public static Account withoutId(Money balance){
        return new Account(null, balance, false, KYCLevel.SILVER);
    }

    public static Account withId(String accountId, Money balance, boolean active, KYCLevel kycLevel) {
        return new Account(accountId, balance, active, kycLevel);
    }

    public Optional<String> getId() {
        return Optional.ofNullable(this.accountId);
    }

    public Money getBalance() {
        return balance;
    }

    private void updateAccountBalance(Money money) {
        this.balance = money;
    }

    public boolean withdraw(Money money, Money threshold) {
        if (!this.active) {
            throw new TransactionProcessException(ResponseCode.SOURCE_ACCOUNT_INACTIVE);
        }
        if (!mayWithdraw(money, threshold)) {
            throw new TransactionProcessException(ResponseCode.INSUFFICIENT_FUND);
        }

        Money newBalance = balance.minus(money);
        updateAccountBalance(newBalance);
        return true;
    }

    public boolean deposit(Money money) {
        Money newBalance = balance.plus(money);
        updateAccountBalance(newBalance);
        return true;
    }

    private boolean mayWithdraw(Money money, Money threshold) {
        if (kycLevel.equals(KYCLevel.SILVER) && money.isGreaterThan(threshold)) {
            throw new TransactionProcessException(ResponseCode.THRESHOLD_EXCEEDED);
        }
        return Money.add(balance, money.negate())
                .isPositiveOrZero();
    }
}