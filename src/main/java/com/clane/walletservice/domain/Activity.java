package com.clane.walletservice.domain;

import com.clane.walletservice.util.GeneratorUtil;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @author victor abidoye
 */

/**
 * A money transfer activity between {@link Account}s.
 * Activity is mapped to transaction
 */
@Value
@RequiredArgsConstructor
@Builder
public class Activity {
    @Getter
    private final Long id;

    /**
     * The debited account.
     */
    @Getter
    @NotNull
    private final Account sourceAccount;

    /**
     * The credited account.
     */
    @Getter
    @NotNull
    private final Account targetAccount;

    /**
     * The timestamp of the activity.
     */
    @Getter
    @NotNull
    private final LocalDateTime timestamp;

    /**
     * The money that was transferred between the accounts.
     */
    @Getter
    @NotNull
    private final Money money;

    @Getter
    @NotNull
    private final TransactionStatus status;

    @Getter
    private String transactionRef = GeneratorUtil.generateTransactionRef();

    public Activity(
            @NotNull Account sourceAccount,
            @NonNull Account targetAccount,
            @NotNull LocalDateTime timestamp,
            @NotNull Money money,
            @NotNull TransactionStatus status){
        this.id = null;
        this.sourceAccount = sourceAccount;
        this.targetAccount = targetAccount;
        this.timestamp = timestamp;
        this.money = money;
        this.status = status;
    }
}