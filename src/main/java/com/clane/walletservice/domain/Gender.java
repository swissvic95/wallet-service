package com.clane.walletservice.domain;

public enum Gender {
    MALE, FEMALE
}
