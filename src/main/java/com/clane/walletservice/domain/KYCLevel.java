package com.clane.walletservice.domain;

public enum KYCLevel {
    SILVER, GOLD
}
