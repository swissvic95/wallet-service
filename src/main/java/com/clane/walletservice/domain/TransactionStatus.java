package com.clane.walletservice.domain;

public enum TransactionStatus {
    SUCCESSFUL, PENDING, FAILED
}
