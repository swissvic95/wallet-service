package com.clane.walletservice.domain;

import com.clane.walletservice.dataaccess.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author victor abidoye
 */
@Data
@AllArgsConstructor
public class UserConfirmation {
    private User user;
    private String confirmationData;
    private String confirmationLink;
}
