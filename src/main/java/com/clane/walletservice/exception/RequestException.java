package com.clane.walletservice.exception;

import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author victor abidoye
 */
@NoArgsConstructor
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class RequestException extends RuntimeException{
    public RequestException(String msg) {
        super(msg);
    }
}
