package com.clane.walletservice.exception;

import com.clane.walletservice.controller.response.ResponseCode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author victor abidoye
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
@AllArgsConstructor
@Data
public class TransactionProcessException extends RuntimeException{

    private ResponseCode responseCode;
}
