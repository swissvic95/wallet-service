package com.clane.walletservice.service;

import com.clane.walletservice.controller.request.TopUpRequest;
import com.clane.walletservice.controller.response.Response;
import com.clane.walletservice.controller.response.WalletAccountResponse;
import com.github.javafaker.Bool;
import reactor.core.publisher.Mono;

/**
 * @author victor abidoye
 */
public interface AccountManagementService {
    Mono<Response> topUpAccount(TopUpRequest request);
    Mono<WalletAccountResponse> getWalletAccountById(String walletId);
}
