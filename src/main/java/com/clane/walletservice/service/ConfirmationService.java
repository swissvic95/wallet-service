package com.clane.walletservice.service;

import com.clane.walletservice.dataaccess.model.User;
import com.clane.walletservice.domain.UserConfirmation;
import reactor.core.publisher.Mono;

/**
 * @author victor abidoye
 */
public interface ConfirmationService {
    UserConfirmation buildRegistrationConfirmationData(User user, String redirectUrl);
    Mono<String> getEmailFromConfirmationData(String data);
}
