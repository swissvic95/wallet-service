package com.clane.walletservice.service.Impl;

import com.clane.walletservice.controller.request.TopUpRequest;
import com.clane.walletservice.controller.response.Response;
import com.clane.walletservice.controller.response.ResponseCode;
import com.clane.walletservice.controller.response.WalletAccountResponse;
import com.clane.walletservice.dataaccess.AccountMapper;
import com.clane.walletservice.dataaccess.repository.WalletAccountRepository;
import com.clane.walletservice.domain.Money;
import com.clane.walletservice.exception.NotFoundException;
import com.clane.walletservice.service.AccountManagementService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/**
 * @author victor abidoye
 *
 * Class is dedicated towards the management of the wallet account.
 */
@Service
@Slf4j
@AllArgsConstructor
public class AccountManagementServiceImpl implements AccountManagementService {

    private final ModelMapper modelMapper = new ModelMapper();
    private WalletAccountRepository walletAccountRepository;
    private AccountMapper accountMapper;

    /**
     * Method adds amount to wallet account balance
     * @param request topRequest request object
     * @return Mono<Response>
     * @throws NotFoundException when account is not found
     * */
    @Override
    public Mono<Response> topUpAccount(TopUpRequest request) {
        return walletAccountRepository.loadWalletAccount(request.getWalletAccountId(), request.getEmail())
                .switchIfEmpty(Mono.error(new NotFoundException("Account not found")))
                .flatMap(walletAccount -> Mono.just(accountMapper.mapToAccountDomainEntity(walletAccount)))
                .flatMap(accountDomain -> {
                    Money money = Money.of(request.getAmount());
                    accountDomain.deposit(money);

                    return walletAccountRepository.updateWalletAccountBalance(accountDomain.getBalance().getAmount()
                            .longValue(), accountDomain.getId().get());
                })
                .flatMap(update -> {
                    Response response = new Response();
                    response.setResponseCode(ResponseCode.SUCCESSFUL.getResponseCode());
                    response.setResponseDescription(ResponseCode.SUCCESSFUL.getResponseMessage());

                    return Mono.just(response);
                });

    }

    /**
     * @param walletId the wallet accountID
     * @return Mono<WalletAccountResponse>
     * @throws NotFoundException when account wallet is not found
     * **/
    @Override
    public Mono<WalletAccountResponse> getWalletAccountById(String walletId) {
        return walletAccountRepository.loadWalletAccountById(walletId)
                .switchIfEmpty(Mono.error(new NotFoundException("Account not found")))
                .flatMap(account -> {
                    WalletAccountResponse response = modelMapper.map(account, WalletAccountResponse.class);

                    return Mono.just(response);
                });
    }
}
