package com.clane.walletservice.service.Impl;

import com.clane.walletservice.config.AppProperties;
import com.clane.walletservice.dataaccess.dao.CacheDao;
import com.clane.walletservice.exception.NotFoundException;
import com.clane.walletservice.exception.RequestException;
import com.clane.walletservice.dataaccess.model.User;
import com.clane.walletservice.domain.UserConfirmation;
import com.clane.walletservice.service.ConfirmationService;
import com.clane.walletservice.util.GeneratorUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;

/**
 * @author victor abidoye
 */
@Service
@AllArgsConstructor
@Slf4j
public class ConfirmationServiceImpl implements ConfirmationService {

    private AppProperties appProperties;
    private CacheDao cacheDao;

    /**
     * method generate registration confirmation link to complete user registration
     * caches the confirmation data on redis
     *
     * @param user user object
     * @param redirectUrl the base url for the construction of the confirmation link
     * @return UserConfirmation
     * */
    @Override
    public UserConfirmation buildRegistrationConfirmationData(User user, String redirectUrl) {
        try {
            String generatedData = GeneratorUtil.generateConfirmationData();
            if (!StringUtils.hasText(redirectUrl)) {
                redirectUrl = appProperties.getRedirectUrl();
            }
            if (redirectUrl.charAt(redirectUrl.length() - 1) == '/') {
                redirectUrl = redirectUrl.substring(0, redirectUrl.length() - 1);
            }

            redirectUrl = redirectUrl + "/" + generatedData;
            log.info("Confirmation data - {}", redirectUrl);

            UserConfirmation userConfirmation = new UserConfirmation(user, generatedData, redirectUrl);
            cacheDao.cacheConfirmationData(userConfirmation)
                    .subscribe();

            return userConfirmation;

        }
        catch (Exception e){
            log.error("Error while generating registration confirmation link - ", e);
            throw new RequestException("Error occurred, please try again");
        }
    }

    /**
     * This function returns the email of the user for activation
     * It checks if the confirmation data exist in the cache, extracts the email,
     * and then deletes it from the cache
     *
     * @param confirmationData confirmation data appended to the confirmation link
     * @return it returns the user's email
     * @throws NotFoundException if confirmationData is not found in the cache
     * */
    @Override
    public Mono<String> getEmailFromConfirmationData(String confirmationData) {
        return cacheDao.get(confirmationData)
                .switchIfEmpty(Mono.error(new NotFoundException("Confirmation data is invalid")))
                .flatMap(cachedData -> {
                    cacheDao.remove(confirmationData).subscribe();
                    return Mono.just(String.valueOf(cachedData));
                });
    }
}
