package com.clane.walletservice.service.Impl;

import com.clane.walletservice.domain.UserConfirmation;
import com.clane.walletservice.service.NotificationService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/**
 * @author victor abidoye
 */
@Service
public class EmailNotificationService implements NotificationService {

    @Override
    public Mono<Boolean> sendMessage(UserConfirmation message) {
        return Mono.just(true);
    }
}
