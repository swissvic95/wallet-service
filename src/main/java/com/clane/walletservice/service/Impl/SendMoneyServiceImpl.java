package com.clane.walletservice.service.Impl;

import com.clane.walletservice.config.WalletTransferConfiguration;
import com.clane.walletservice.controller.request.SendMoneyRequest;
import com.clane.walletservice.controller.response.TransactionResponse;
import com.clane.walletservice.dataaccess.AccountMapper;
import com.clane.walletservice.dataaccess.model.Transaction;
import com.clane.walletservice.dataaccess.model.WalletAccount;
import com.clane.walletservice.dataaccess.repository.TransactionRepository;
import com.clane.walletservice.dataaccess.repository.WalletAccountRepository;
import com.clane.walletservice.domain.Account;
import com.clane.walletservice.domain.Activity;
import com.clane.walletservice.domain.Money;
import com.clane.walletservice.exception.NotFoundException;
import com.clane.walletservice.exception.TransactionProcessException;
import com.clane.walletservice.service.SendMoneyService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;
import java.time.LocalDateTime;

/**
 * @author victor abidoye
 */
@Service
@Slf4j
@AllArgsConstructor
public class SendMoneyServiceImpl implements SendMoneyService {

    private final ModelMapper modelMapper = new ModelMapper();
    private WalletAccountRepository walletAccountRepository;
    private TransactionRepository transactionRepository;
    private WalletTransferConfiguration transferConfiguration;
    private AccountMapper accountMapper;


    /**
    * The method is designed to send money from one wallet account to the other
     * @param request SendMoneyRequest
     * @return Mono<TransactionResponse>
     * @throws NotFoundException when either the source or target account is not found.
     * @throws TransactionProcessException when source account is inactive or for insufficient fund
    * **/
    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Mono<TransactionResponse> sendMoney(SendMoneyRequest request) {
        Mono<WalletAccount> sourceWalletAccount = walletAccountRepository.loadWalletAccount(request.getSourceWalletAccountId(),
                request.getSourceEmail())
                .switchIfEmpty(Mono.error(new NotFoundException("Source Account not found")));

        Mono<WalletAccount> targetWalletAccount = walletAccountRepository.loadWalletAccount(request.getTargetWalletAccountId(),
                request.getTargetEmail())
                .switchIfEmpty(Mono.error(new NotFoundException("Target Account not found")));

        Money transferMoney = Money.of(request.getAmount());

        return sourceWalletAccount.zipWith(targetWalletAccount)
                .flatMap(wallets -> {
                    Account sourceAccount = accountMapper.mapToAccountDomainEntity(wallets.getT1());
                    Account targetAccount = accountMapper.mapToAccountDomainEntity(wallets.getT2());
                    Activity activity = Activity.builder()
                            .sourceAccount(sourceAccount)
                            .targetAccount(targetAccount)
                            .money(transferMoney)
                            .timestamp(LocalDateTime.now())
                            .build();
                    return Mono.just(activity);
                })
                .flatMap(activity -> {
                    activity.getSourceAccount().withdraw(transferMoney, transferConfiguration
                            .getKYCSilverTransferThreshold());
                    activity.getTargetAccount().deposit(transferMoney);

                    return Mono.just(activity);
                })
                .flatMap(activity -> {
                    Long sourceAccountBalance = activity.getSourceAccount().getBalance().getAmount().longValue();
                    Long targetAccountBalance = activity.getTargetAccount().getBalance().getAmount().longValue();

                    walletAccountRepository.updateWalletAccountBalance(sourceAccountBalance,
                            activity.getSourceAccount().getId().get()).subscribe();

                    walletAccountRepository.updateWalletAccountBalance(targetAccountBalance,
                            activity.getTargetAccount().getId().get())
                            .subscribe();

                    Transaction transaction = accountMapper.mapToTransactionJpaEntity(activity);
                    return Mono.just(transaction);
                })
                .flatMap(transaction -> transactionRepository.addTransaction(transaction))
                .flatMap(transaction -> {
                    TransactionResponse response = modelMapper.map(transaction, TransactionResponse.class);
                    return Mono.just(response);
                });
    }

}
