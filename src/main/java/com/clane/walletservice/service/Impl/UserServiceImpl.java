package com.clane.walletservice.service.Impl;

import com.clane.walletservice.controller.request.UserProfilePayload;
import com.clane.walletservice.controller.response.Response;
import com.clane.walletservice.controller.response.ResponseCode;
import com.clane.walletservice.controller.response.UserProfileResponse;
import com.clane.walletservice.dataaccess.model.WalletAccount;
import com.clane.walletservice.dataaccess.repository.UserRepository;
import com.clane.walletservice.exception.DuplicateRecordException;
import com.clane.walletservice.exception.NotFoundException;
import com.clane.walletservice.exception.RequestException;
import com.clane.walletservice.domain.KYCLevel;
import com.clane.walletservice.dataaccess.model.User;
import com.clane.walletservice.domain.UserConfirmation;
import com.clane.walletservice.service.ConfirmationService;
import com.clane.walletservice.service.NotificationService;
import com.clane.walletservice.service.UserService;
import com.clane.walletservice.util.GeneratorUtil;
import com.clane.walletservice.util.SecurityUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * @author victor abidoye
 */

@Service
@AllArgsConstructor
@Slf4j
@Transactional
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private NotificationService notificationService;
    private ConfirmationService confirmationService;

    private final ModelMapper modelMapper = new ModelMapper();

    /**
     * method registers new user on the wallet system
     * @param request user profile data
     * @return Mono<UserRegistrationResponse> returns mono of userRegistrationResponse
     * @throws DuplicateRecordException if user with the same email address exist
     *
     * */
    @Override
    public Mono<UserProfileResponse> registerNewUser(UserProfilePayload request) {
        return userRepository.getUserByEmail(request.getEmail())
                .flatMap(user -> Mono.just(Optional.of(user)))
                .defaultIfEmpty(Optional.empty())
                .flatMap(optionalUser -> {
                    if (optionalUser.isPresent()) {
                        return Mono.error(new DuplicateRecordException("Email address already exist on the wallet service"));
                    }
                    String hashedPass = hashPassword(request.getPassword());
                    User user = modelMapper.map(request, User.class);
                    user.setHashedPassword(hashedPass);
                    user.setKycLevel(KYCLevel.SILVER);

                    return Mono.just(user);
                })
                .flatMap(user -> {
                    String walletId = GeneratorUtil.generateWalletId();
                    WalletAccount walletAccount = new WalletAccount();
                    walletAccount.setWalletAccountId(walletId);
                    walletAccount.setActive(false);
                    walletAccount.setDateTimeCreated(LocalDateTime.now());
                    user.setWalletAccount(walletAccount);

                    return userRepository.addUser(user);
                })
                .flatMap(user -> {
                    UserProfileResponse response = modelMapper.map(user, UserProfileResponse.class);
                    response.setWalletId(user.getWalletAccount().getWalletAccountId());

                    UserConfirmation confirmationData = confirmationService.buildRegistrationConfirmationData(user,
                            request.getRedirectUrl());

                    notificationService.sendMessage(confirmationData)
                            .subscribe();

                    return Mono.just(response);
                });
    }

    /**
     * method update user profile data on the wallet system
     * @param request user profile data
     * @return Mono<UserRegistrationResponse> returns mono of userRegistrationResponse
     * @throws NotFoundException if user does not exit
     *
     * */
    @Override
    public Mono<UserProfileResponse> updateUserProfile(UserProfilePayload request) {
        return userRepository.getUserByEmail(request.getEmail())
                .switchIfEmpty(Mono.error(new NotFoundException()))
                .flatMap(user -> {
                    modelMapper.map(request, user);
                    user.setKycLevel(KYCLevel.GOLD);

                    return userRepository.update(user);
                })
                .flatMap(user -> Mono.just(modelMapper.map(user, UserProfileResponse.class)));
    }

    /**
     * @param confirmationData data appended to the confirmation link for wallet activation
     * @return Mono<Response> method returns response object
     * @throws NotFoundException if user does not exist
     * */
    @Override
    public Mono<Response> activateWalletUsingUserConfirmationData(String confirmationData) {
        return confirmationService.getEmailFromConfirmationData(confirmationData)
                .flatMap(email -> userRepository.getUserByEmail(email))
                .switchIfEmpty(Mono.error(new NotFoundException("User not found on the wallet system")))
                .flatMap(user -> {
                    user.getWalletAccount().setActive(true);
                    userRepository.update(user)
                            .log()
                            .subscribe();

                    Response response = new Response();
                    response.setResponseCode(ResponseCode.SUCCESSFUL.getResponseCode());
                    response.setResponseDescription(ResponseCode.SUCCESSFUL.getResponseMessage());

                    return Mono.just(response);
                });
    }

    @Override
    public Mono<UserProfileResponse> getUserByEmail(String email) {
        return userRepository.getUserByEmail(email)
                .switchIfEmpty(Mono.error(new NotFoundException("User not found on the wallet system")))
                .flatMap(user -> {
                    UserProfileResponse response = modelMapper.map(user, UserProfileResponse.class);
                    return Mono.just(response);
                });
    }


    private String hashPassword(String password){
        try {
            return SecurityUtil.hash(password);
        }
        catch (Exception e) {
            log.error("Error occurred - ", e);
            throw new RequestException("Error processing request");
        }
    }
}