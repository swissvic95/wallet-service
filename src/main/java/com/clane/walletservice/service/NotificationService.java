package com.clane.walletservice.service;

import com.clane.walletservice.domain.UserConfirmation;
import reactor.core.publisher.Mono;

/**
 * @author victor abidoye
 */
public interface NotificationService {
    Mono<Boolean> sendMessage(UserConfirmation message);
}
