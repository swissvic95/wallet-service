package com.clane.walletservice.service;

import com.clane.walletservice.controller.request.SendMoneyRequest;
import com.clane.walletservice.controller.response.TransactionResponse;
import reactor.core.publisher.Mono;

/**
 * @author victor abidoye
 */
public interface SendMoneyService {
    Mono<TransactionResponse> sendMoney(SendMoneyRequest request);
}
