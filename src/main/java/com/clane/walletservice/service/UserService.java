package com.clane.walletservice.service;

import com.clane.walletservice.controller.request.UserProfilePayload;
import com.clane.walletservice.controller.response.Response;
import com.clane.walletservice.controller.response.UserProfileResponse;
import reactor.core.publisher.Mono;

/**
 * @author victor abidoye
 */
public interface UserService {
    Mono<UserProfileResponse> registerNewUser(UserProfilePayload request);
    Mono<UserProfileResponse> updateUserProfile(UserProfilePayload request);
    Mono<Response> activateWalletUsingUserConfirmationData(String data);
    Mono<UserProfileResponse> getUserByEmail(String email);
}
