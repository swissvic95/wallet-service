package com.clane.walletservice.util;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;

/**
 * @author victor abidoye
 */
public class GeneratorUtil {

    public static String generateWalletId() {
        return RandomStringUtils.randomNumeric(10);
    }

    public static String generateConfirmationData() {
        return DigestUtils.md5DigestAsHex(RandomStringUtils.
                randomAlphanumeric(50).getBytes());
    }

    public static String generateTransactionRef() {
        return DigestUtils.md5DigestAsHex(RandomStringUtils.
                randomAlphanumeric(20).getBytes());
    }
}
