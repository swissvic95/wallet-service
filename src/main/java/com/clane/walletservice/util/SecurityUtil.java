package com.clane.walletservice.util;

import com.clane.walletservice.exception.RequestException;
import lombok.extern.slf4j.Slf4j;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

/**
 * @author victor abidoye
 */
@Slf4j
public class SecurityUtil {

    private static final String cipher = "AES/ECB/PKCS5Padding";
    private static final String aesEncryptionAlgo = "AES";
    private static SecretKeySpec secretKey;
    private static byte[] keyByte;

    private static void setSecurityKey(String key) throws UnsupportedEncodingException, NoSuchAlgorithmException{
        MessageDigest sha = null;
        keyByte = key.getBytes("UTF-8");
        sha = MessageDigest.getInstance("SHA-1");
        keyByte = sha.digest(keyByte);
        keyByte = Arrays.copyOf(keyByte, 16);
        secretKey = new SecretKeySpec(keyByte, "AES");
    }

    public static String encrypt(String plainText, String key) throws Exception{
        setSecurityKey(key);
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        return Base64.getEncoder().encodeToString(cipher.doFinal(plainText.getBytes("UTF-8")));
    }

    public static String decrypt(String encryptedText, String key) throws Exception {
        setSecurityKey(key);
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        return new String(cipher.doFinal(Base64.getDecoder().decode(encryptedText)));
    }

    public static String hash(String plainText) throws Exception{
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        plainText = plainText.trim();
        messageDigest.update(plainText.getBytes(StandardCharsets.UTF_8));
        return new BigInteger(1, messageDigest.digest()).toString(16);
    }
}
