package com.clane.walletservice.validation;

import com.clane.walletservice.validation.annotation.ValidEnumValue;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author victor abidoye
 */
public class EnumValidator implements ConstraintValidator<ValidEnumValue, CharSequence> {

    private List<String> acceptedValues;

    @Override
    public void initialize(ValidEnumValue constraintAnnotation) {
        acceptedValues = Stream.of(constraintAnnotation.enumClass().getEnumConstants())
                .map(Enum::name)
                .collect(Collectors.toList());
    }

    @Override
    public boolean isValid(CharSequence charSequence, ConstraintValidatorContext constraintValidatorContext) {
        if (charSequence == null) {
            return true;
        }
        return acceptedValues.contains(charSequence.toString());
    }

}
