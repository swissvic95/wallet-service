package com.clane.walletservice.validation;

import com.clane.walletservice.validation.annotation.ValidNonNullBetweenTwoFields;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

/**
 * @author victor abidoye
 */
@Slf4j
public class NotNullIfTheOtherFieldIsNullValidator implements ConstraintValidator<ValidNonNullBetweenTwoFields, Object> {

    private String fieldNames[];

    @Override
    public void initialize(ValidNonNullBetweenTwoFields constraintAnnotation) {
        this.fieldNames = constraintAnnotation.fieldNames();
    }

    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext constraintValidatorContext) {
        if (obj == null) {
            return false;
        }

        try {
            String value1 = String.valueOf(PropertyUtils.getProperty(obj,  fieldNames[0]));
            String value2 = String.valueOf(PropertyUtils.getProperty(obj, fieldNames[1]));
            if (!(Objects.nonNull(value1) || Objects.nonNull(value2))) {
                return false;
            }

            if (!(hasText(value1) || hasText(value1))) {
                return false;
            }
            return true;
        }
        catch (Exception e) {
            log.error("Error occurred during validation - {}", e);
            return false;
        }
    }

    private boolean hasText(String text) {
        return StringUtils.hasText(text) && !text.equalsIgnoreCase("null");
    }
}
