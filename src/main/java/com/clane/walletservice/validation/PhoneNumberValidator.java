package com.clane.walletservice.validation;

import com.clane.walletservice.validation.annotation.ValidPhoneNumber;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author victor abidoye
 */
@Slf4j
public class PhoneNumberValidator implements ConstraintValidator<ValidPhoneNumber, String> {

    private static PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

    @Override
    public boolean isValid(String phoneNumberStr, ConstraintValidatorContext constraintValidatorContext) {
        try {
            Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(phoneNumberStr, "NG");
            return phoneNumberUtil.isValidNumber(phoneNumber);
        }
        catch (Exception e) {
            log.error("Phone Number Validation Error");
            return false;
        }
    }
}
