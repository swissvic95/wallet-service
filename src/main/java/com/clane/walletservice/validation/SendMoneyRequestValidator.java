package com.clane.walletservice.validation;

import com.clane.walletservice.validation.annotation.ValidNonNullBetweenTwoFields;
import com.clane.walletservice.validation.annotation.ValidSendMoneyRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

/**
 * @author victor abidoye
 */
@Slf4j
public class SendMoneyRequestValidator implements ConstraintValidator<ValidSendMoneyRequest, Object> {

    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext constraintValidatorContext) {
        if (obj == null) {
            return false;
        }
        try {
            String sourceEmail = String.valueOf(PropertyUtils.getProperty(obj,  "sourceEmail"));
            String sourceWalletAccountId = String.valueOf(PropertyUtils.getProperty(obj, "sourceWalletAccountId"));
            if (!(Objects.nonNull(sourceEmail) || Objects.nonNull(sourceWalletAccountId))) {
                return false;
            }

            if (!(hasText(sourceEmail) || hasText(sourceWalletAccountId))) {
                return false;
            }

            String targetEmail = String.valueOf(PropertyUtils.getProperty(obj,  "targetEmail"));
            String targetWalletAccountId = String.valueOf(PropertyUtils.getProperty(obj, "targetWalletAccountId"));
            if (!(Objects.nonNull(targetEmail) || Objects.nonNull(targetWalletAccountId))) {
                return false;
            }

            if (!(hasText(targetEmail) || hasText(targetWalletAccountId))) {
                return false;
            }
            return true;
        }
        catch (Exception e) {
            log.error("Error occurred during validation - {}", e);
            return false;
        }
    }

    @Override
    public void initialize(ValidSendMoneyRequest constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    private boolean hasText(String text) {
        return StringUtils.hasText(text) && !text.equalsIgnoreCase("null");
    }
}
