package com.clane.walletservice.validation;

/**
 * @author victor abidoye
 */
public class ValidationGroup {
    public interface ValidateUserCreationRequest {};
    public interface ValidateUpdateUserRecordRequest{};
    public interface ValidateGetUserByEmail{};
}
