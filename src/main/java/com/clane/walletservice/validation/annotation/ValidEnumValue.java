package com.clane.walletservice.validation.annotation;

import com.clane.walletservice.validation.EnumValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = EnumValidator.class)
public @interface ValidEnumValue {
    Class<? extends Enum<?>> enumClass();
    String message() default "Incorrect value";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}