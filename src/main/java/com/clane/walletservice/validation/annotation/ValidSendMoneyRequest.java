package com.clane.walletservice.validation.annotation;

import com.clane.walletservice.controller.request.SendMoneyRequest;
import com.clane.walletservice.validation.NotNullIfTheOtherFieldIsNullValidator;
import com.clane.walletservice.validation.SendMoneyRequestValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author victor abidoye
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = SendMoneyRequestValidator.class)
public @interface ValidSendMoneyRequest {
    String message() default "";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
