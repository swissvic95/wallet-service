package com.clane.walletservice.repository;

import com.clane.walletservice.BaseTest;
import com.clane.walletservice.dataaccess.repository.UserRepository;
import com.clane.walletservice.dataaccess.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.test.StepVerifier;

@SpringBootTest
public class UserRepositoryTest extends BaseTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testCreateUser() {
        User user = new User();
        user.setFirstName(faker.name().firstName());
        user.setLastName(faker.name().lastName());
        user.setEmail(faker.internet().emailAddress());
        user.setPhoneNumber(faker.phoneNumber().phoneNumber());

        StepVerifier.create(userRepository.addUser(user))
                .expectNextMatches(createdUser -> createdUser.getId() > 0)
                .verifyComplete();
    }
}
