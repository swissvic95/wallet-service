package com.clane.walletservice.service;

import com.clane.walletservice.BaseTest;
import com.clane.walletservice.config.WalletTransferConfiguration;
import com.clane.walletservice.controller.request.SendMoneyRequest;
import com.clane.walletservice.controller.response.ResponseCode;
import com.clane.walletservice.dataaccess.AccountMapper;
import com.clane.walletservice.dataaccess.model.Transaction;
import com.clane.walletservice.dataaccess.model.User;
import com.clane.walletservice.dataaccess.model.WalletAccount;
import com.clane.walletservice.dataaccess.repository.TransactionRepository;
import com.clane.walletservice.dataaccess.repository.WalletAccountRepository;
import com.clane.walletservice.domain.KYCLevel;
import com.clane.walletservice.exception.NotFoundException;
import com.clane.walletservice.exception.TransactionProcessException;
import com.clane.walletservice.service.Impl.SendMoneyServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.UUID;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * @author victor abidoye
 */

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SendMoneyServiceTest extends BaseTest {

    @Mock
    private WalletAccountRepository walletAccountRepository;
    private AccountMapper accountMapper = new AccountMapper();
    @Mock
    private TransactionRepository transactionRepository;

    private WalletTransferConfiguration transferConfiguration = new WalletTransferConfiguration();

    @InjectMocks
    private SendMoneyService sendMoneyService;

    @BeforeAll
    public void init() {
        sendMoneyService = new SendMoneyServiceImpl(walletAccountRepository, transactionRepository,
                transferConfiguration, accountMapper);
    }

    @Test
    public void testSendMoneyWhenSourceAccountDoesNotExist() {
        SendMoneyRequest request = new SendMoneyRequest();
        String sourceEmail = faker.internet().emailAddress();
        String targetEmail = faker.internet().emailAddress();
        request.setSourceEmail(sourceEmail);
        request.setTargetEmail(targetEmail);
        request.setAmount(100L);
        when(walletAccountRepository.loadWalletAccount(any(), any()))
                .thenReturn(Mono.empty());
        StepVerifier.create(sendMoneyService.sendMoney(request))
                .expectError(NotFoundException.class)
                .verify();
    }

    @Test
    public void testSendMoneyWhenTargetAccountDoesNotExist() {
        SendMoneyRequest request = new SendMoneyRequest();
        String sourceEmail = faker.internet().emailAddress();
        String targetEmail = faker.internet().emailAddress();
        request.setSourceEmail(sourceEmail);
        request.setTargetEmail(targetEmail);
        request.setAmount(100L);
        WalletAccount walletAccount = getWalletAccount(0L, true);
        when(walletAccountRepository.loadWalletAccount(any(), any()))
                .thenReturn(Mono.just(walletAccount))
                .thenReturn(Mono.empty());
        StepVerifier.create(sendMoneyService.sendMoney(request))
                .expectErrorMatches(e -> e.getMessage().equals("Target Account not found"))
                .verify();
    }

    @Test
    public void testSuccessfulMoneyTransferBetweenTwoWallets() {
        SendMoneyRequest request = new SendMoneyRequest();
        String sourceEmail = faker.internet().emailAddress();
        String targetEmail = faker.internet().emailAddress();
        request.setSourceEmail(sourceEmail);
        request.setTargetEmail(targetEmail);
        request.setAmount(100L);
        WalletAccount sourceWalletAccount = getWalletAccount(100L, true);
        WalletAccount targetWalletAccount = getWalletAccount(0L, true);

        Transaction transaction = new Transaction();

        when(walletAccountRepository.loadWalletAccount(any(), any()))
                .thenReturn(Mono.just(sourceWalletAccount))
                .thenReturn(Mono.just(targetWalletAccount));
        when(walletAccountRepository.updateWalletAccountBalance(any(), any()))
                .thenReturn(Mono.just(true))
                .thenReturn(Mono.just(true));
        when(transactionRepository.addTransaction(any())).thenReturn(Mono.just(transaction));

        StepVerifier.create(sendMoneyService.sendMoney(request))
                .expectNextMatches(response -> null != response)
                .verifyComplete();
    }

    @Test
    public void testSendMoneyWhenAmountExceedsTheMaximumThresholdForKYCLevel() {
        SendMoneyRequest request = new SendMoneyRequest();
        String sourceEmail = faker.internet().emailAddress();
        String targetEmail = faker.internet().emailAddress();
        request.setSourceEmail(sourceEmail);
        request.setTargetEmail(targetEmail);
        request.setAmount(60000L);
        WalletAccount sourceWalletAccount = getWalletAccount(100L, true);
        WalletAccount targetWalletAccount = getWalletAccount(0L, true);

        when(walletAccountRepository.loadWalletAccount(any(), any()))
                .thenReturn(Mono.just(sourceWalletAccount))
                .thenReturn(Mono.just(targetWalletAccount));

        StepVerifier.create(sendMoneyService.sendMoney(request))
                .expectErrorMatches( e ->  ((TransactionProcessException) e).getResponseCode()
                        .equals(ResponseCode.THRESHOLD_EXCEEDED))
                .verify();
    }

    @Test
    public void testSendMoneyWhenThereIsAnInsufficientFund() {
        SendMoneyRequest request = new SendMoneyRequest();
        String sourceEmail = faker.internet().emailAddress();
        String targetEmail = faker.internet().emailAddress();
        request.setSourceEmail(sourceEmail);
        request.setTargetEmail(targetEmail);
        request.setAmount(100L);
        WalletAccount sourceWalletAccount = getWalletAccount(0L, true);
        WalletAccount targetWalletAccount = getWalletAccount(0L, true);

        when(walletAccountRepository.loadWalletAccount(any(), any()))
                .thenReturn(Mono.just(sourceWalletAccount))
                .thenReturn(Mono.just(targetWalletAccount));

        StepVerifier.create(sendMoneyService.sendMoney(request))
                .expectErrorMatches( e ->  ((TransactionProcessException) e).getResponseCode()
                        .equals(ResponseCode.INSUFFICIENT_FUND))
                .verify();
    }

    @Test
    public void testSendMoneyWhenSourceAccountIsInActive() {
        SendMoneyRequest request = new SendMoneyRequest();
        String sourceEmail = faker.internet().emailAddress();
        String targetEmail = faker.internet().emailAddress();
        request.setSourceEmail(sourceEmail);
        request.setTargetEmail(targetEmail);
        request.setAmount(100L);
        WalletAccount sourceWalletAccount = getWalletAccount(500L, false);
        WalletAccount targetWalletAccount = getWalletAccount(0L, true);

        when(walletAccountRepository.loadWalletAccount(any(), any()))
                .thenReturn(Mono.just(sourceWalletAccount))
                .thenReturn(Mono.just(targetWalletAccount));

        StepVerifier.create(sendMoneyService.sendMoney(request))
                .expectErrorMatches( e ->  ((TransactionProcessException) e).getResponseCode()
                        .equals(ResponseCode.SOURCE_ACCOUNT_INACTIVE))
                .verify();
    }

    public WalletAccount getWalletAccount(Long amount, boolean active) {
        WalletAccount walletAccount = new WalletAccount();
        walletAccount.setBalance(amount);
        walletAccount.setActive(active);
        walletAccount.setWalletAccountId(UUID.randomUUID().toString());

        User user = new User();
        user.setKycLevel(KYCLevel.SILVER);

        walletAccount.setUser(user);

        return walletAccount;
    }
}
