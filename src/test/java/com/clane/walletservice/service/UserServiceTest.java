package com.clane.walletservice.service;

import com.clane.walletservice.BaseTest;
import com.clane.walletservice.controller.request.UserProfilePayload;
import com.clane.walletservice.controller.response.ResponseCode;
import com.clane.walletservice.dataaccess.model.WalletAccount;
import com.clane.walletservice.dataaccess.repository.UserRepository;
import com.clane.walletservice.domain.UserConfirmation;
import com.clane.walletservice.exception.DuplicateRecordException;
import com.clane.walletservice.exception.NotFoundException;
import com.clane.walletservice.dataaccess.model.User;
import com.clane.walletservice.service.Impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UserServiceTest extends BaseTest {
    @Mock
    private UserRepository userRepository;
    @Mock
    private NotificationService notificationService;
    @Mock
    private ConfirmationService confirmationService;

    @InjectMocks
    private UserService userService;

    @BeforeAll
    public void init() {
        userService = new UserServiceImpl(userRepository, notificationService, confirmationService);
    }

    @Test
    public void registerUserWhenUserDoesNotExist() {
        String firstName = faker.name().firstName();
        String lastName = faker.name().lastName();
        String email = faker.internet().emailAddress();
        String phoneN  = faker.phoneNumber().phoneNumber();

        SampleData sampleData = new SampleData(firstName, lastName, email, phoneN);
        UserProfilePayload request = sampleData.getSampleRequest();
        request.setRedirectUrl("");
        User user = sampleData.getSampleUser();
        WalletAccount walletAccount = new WalletAccount();
        walletAccount.setWalletAccountId(faker.idNumber().toString());
        user.setWalletAccount(walletAccount);
        when(userRepository.getUserByEmail(anyString())).thenReturn(Mono.empty());
        when(userRepository.addUser(any(User.class))).thenReturn(Mono.just(user));
        when(confirmationService.buildRegistrationConfirmationData(any(User.class), anyString()))
                .thenReturn(new UserConfirmation(user, "", ""));
        when(notificationService.sendMessage(any(UserConfirmation.class))).thenReturn(Mono.just(true));

        StepVerifier.create(userService.registerNewUser(request))
                .expectNextMatches(userRes -> userRes.getWalletId() != null)
                .verifyComplete();
    }

    @Test
    public void throwDuplicateExceptionWhenUserWithTheSameEmailAddExist() {
        String firstName = faker.name().firstName();
        String lastName = faker.name().lastName();
        String email = faker.internet().emailAddress();
        String phoneN  = faker.phoneNumber().phoneNumber();

        SampleData sampleData = new SampleData(firstName, lastName, email, phoneN);
        UserProfilePayload request = sampleData.getSampleRequest();
        User user = sampleData.getSampleUser();
        when(userRepository.getUserByEmail(anyString())).thenReturn(Mono.just(user));

        StepVerifier.create(userService.registerNewUser(request))
                .expectError(DuplicateRecordException.class)
                .verify();
    }

    @Test
    public void throwNotFoundExceptionWhenUpdatingProfileForUserWhoDoesNotExist() {
        UserProfilePayload request = new UserProfilePayload();
        request.setEmail(faker.internet().emailAddress());
        when(userRepository.getUserByEmail(anyString())).thenReturn(Mono.empty());
        StepVerifier.create(userService.updateUserProfile(request))
                .expectError(NotFoundException.class)
                .verify();
    }

    @Test
    public void testUpdateUserProfile() {
        String firstName = faker.name().firstName();
        String lastName = faker.name().lastName();
        String email = faker.internet().emailAddress();
        String phoneN  = faker.phoneNumber().phoneNumber();
        UserProfilePayload request = new UserProfilePayload();
        request.setEmail(email);

        SampleData sampleData = new SampleData(firstName, lastName, email, phoneN);
        User user = sampleData.getSampleUser();
        when(userRepository.getUserByEmail(anyString())).thenReturn(Mono.just(user));
        when(userRepository.update(any(User.class))).thenReturn(Mono.just(user));
        StepVerifier.create(userService.updateUserProfile(request))
                .expectNextMatches(updatedUser -> updatedUser.getEmail().equals(email))
                .verifyComplete();
    }

    @Test
    public void testGetUserByEmailWhenUserIsNotFound() {
        when(userRepository.getUserByEmail(anyString())).thenReturn(Mono.empty());
        StepVerifier.create(userService.getUserByEmail(faker.internet().emailAddress()))
                .expectError(NotFoundException.class)
                .verify();
    }

    @Test
    public void testGetUserByEmailWhenUserExists(){
        String firstName = faker.name().firstName();
        String lastName = faker.name().lastName();
        String email = faker.internet().emailAddress();
        String phoneN  = faker.phoneNumber().phoneNumber();
        SampleData sampleData = new SampleData(firstName, lastName, email, phoneN);
        User user = sampleData.getSampleUser();
        when(userRepository.getUserByEmail(anyString())).thenReturn(Mono.just(user));

        StepVerifier.create(userService.getUserByEmail(faker.internet().emailAddress()))
                .expectNextMatches(userProfileResponse -> userProfileResponse.getEmail().equalsIgnoreCase(email))
                .verifyComplete();
    }

    @Test
    public void testActivateWalletUsingUserConfirmationData() {
        String email = faker.internet().emailAddress();
        String confirmationData = faker.lorem().characters();
        User user = new User();
        user.setWalletAccount(new WalletAccount());
        when(confirmationService.getEmailFromConfirmationData(anyString()))
                .thenReturn(Mono.just(email));
        when(userRepository.getUserByEmail(anyString())).thenReturn(Mono.just(user));
        when(userRepository.update(any(User.class))).thenReturn(Mono.just(user));

        StepVerifier.create(userService.activateWalletUsingUserConfirmationData(confirmationData))
                .expectNextMatches(response -> null != response)
                .verifyComplete();
    }

    public class SampleData{
        public UserProfilePayload request;
        public User user;

        public SampleData(String firstName, String lastName, String email, String phoneNumber) {
            user = new User();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmail(email);
            user.setPhoneNumber(phoneNumber);

            request = new UserProfilePayload();
            request.setFirstName(firstName);
            request.setLastName(lastName);
            request.setEmail(email);
            request.setPhoneNumber(phoneNumber);
            request.setPassword(faker.internet().password());
        }

        public UserProfilePayload getSampleRequest() {
            return request;
        }

        public User getSampleUser() {
            return user;
        }

    }
}
