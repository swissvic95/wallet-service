package com.clane.walletservice.util;

import com.clane.walletservice.BaseTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class GeneratorUtilTest extends BaseTest {

    @Test
    public void testGenerateWalletId() {
        String walletId = GeneratorUtil.generateWalletId();

        System.out.println(walletId);
        Assertions.assertNotNull(walletId);
    }
}
