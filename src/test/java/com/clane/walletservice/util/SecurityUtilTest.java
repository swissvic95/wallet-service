package com.clane.walletservice.util;

import com.clane.walletservice.BaseTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SecurityUtilTest extends BaseTest {

    @Test
    public void testHashFn() throws Exception{
        String txt = faker.internet().password();
        String hashedTxt = SecurityUtil.hash(txt);
        Assertions.assertNotEquals(txt, hashedTxt);
    }

    @Test
    public void encryptDecrypt() throws Exception{
        String textToEncrypt = faker.lorem().word();
        String key = "key";
        String encryptedText = SecurityUtil.encrypt(textToEncrypt, key);
        String decryptText = SecurityUtil.decrypt(encryptedText, key);

        Assertions.assertEquals(textToEncrypt, decryptText);
    }
}
